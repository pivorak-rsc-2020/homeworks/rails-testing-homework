# Rails Testing Hometask

Your task is simple - cover model Todo with tests.

## Instructions
1. Install and config Rspec and other required gems
2. Create test file in appropriate folder and with correct name
3. Write your tests there

## 3 mandatory elements to implement in your tests
1. create factory with traits and use it in the test
2. test validations in model
3. use matcher from shoulda-matcher gem

## Additional requirements (1 list item is enough)
1. test scopes in model AND use test double where it's appropriate
OR
2. add test coverage with simplecov gem
OR
3. test todo_controller (methods :index, :create and :show)


## Assesment criterias:
**-1**  :  GitlabCI is red or home task was not submitted before 17.06 (Wednesday), 18:00 PM (Kyiv time)

**0**   :  GitlabCI is green

**1**   : GitlabCI is green *and* your tests are named correctly and put in appropriate folder *and* you used ALL elements from list '3 mandatory elements' (see above)

**2**   : GitlabCI is green *and* you used ALL elements from list '3 mandatory elements' (see above) *and* ONE OF additional requirements. 

## Good news:
Number of tries is not limited within given period of time: 15.06 (after Rails Testing lecture ends) till 17.06 (Wednesday), 18:00 PM (Kyiv time). Don't be shy!

To run tests locally:

`bundle install`

`bundle exec rspec`

To run rubocop locally:

`rubocop`

